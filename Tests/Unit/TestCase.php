<?php

namespace TaylorThomas\WordPress\DekoForms\Tests\Unit;

use Brain\Monkey;

abstract class TestCase extends \PHPUnit\Framework\TestCase
{

  protected function setUp()
  {
    parent::setUp();
    Monkey\setUp();
  }

  protected function tearDown()
  {
    Monkey\tearDown();
    parent::tearDown();
  }
}
