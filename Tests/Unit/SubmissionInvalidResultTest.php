<?php

namespace TaylorThomas\WordPress\DekoForms\Tests\Unit;

use TaylorThomas\WordPress\DekoForms\Factory;

class SubmissionInvalidResultTestCase extends TestCase
{
    const ERRORS = ['email-address' => 'is invalid'];

    protected function getSubmissionInvalidResult()
    {
        return Factory::getSubmissionInvalidResult(self::ERRORS);
    }

    public function testGetResponseCode()
    {
      $this->assertEquals(422, $this->getSubmissionInvalidResult()->getResponseCode());
    }

    public function testGetData()
    {
        $this->assertEquals([
            'errors' => self::ERRORS
        ], $this->getSubmissionInvalidResult()->getData());
    }
}
