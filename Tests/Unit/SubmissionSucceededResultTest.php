<?php

namespace TaylorThomas\WordPress\DekoForms\Tests\Unit;

use TaylorThomas\WordPress\DekoForms\SubmissionSucceededResult;

class SubmissionSucceededResultTestCase extends TestCase
{
    const MESSAGE = 'Hiya!';

    protected function getSubmissionSucceededResult()
    {
        return new SubmissionSucceededResult(self::MESSAGE);
    }

    public function testGetResponseCode()
    {
      $this->assertEquals(200, $this->getSubmissionSucceededResult()->getResponseCode());
    }

    public function testGetData()
    {
        $this->assertEquals([
            'message' => self::MESSAGE
        ], $this->getSubmissionSucceededResult()->getData());
    }
}
