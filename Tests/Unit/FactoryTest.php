<?php

namespace TaylorThomas\WordPress\DekoForms\Tests\Unit;

use TaylorThomas\WordPress\DekoForms\Factory;
use TaylorThomas\WordPress\DekoForms\Constants;

class FactoryTestCase extends TestCase
{
    public function testGetConfirmationReturnsAppropriateClass()
    {
        $this->assertInstanceOf(
            'TaylorThomas\WordPress\DekoForms\LenderConfirmation',
            Factory::getConfirmation(Constants::LENDER_SUBMISSION_FORM_TYPE, [])
        );

        $this->assertInstanceOf(
            'TaylorThomas\WordPress\DekoForms\MerchantConfirmation',
            Factory::getConfirmation(Constants::MERCHANT_SUBMISSION_FORM_TYPE, [])
        );
    }

    public function testGetValidatorReturnsAppropriateClass()
    {
        $this->assertInstanceOf(
            'TaylorThomas\WordPress\DekoForms\LenderSubmissionValidator',
            Factory::getValidator(Constants::LENDER_SUBMISSION_FORM_TYPE, [])
        );

        $this->assertInstanceOf(
            'TaylorThomas\WordPress\DekoForms\MerchantSubmissionValidator',
            Factory::getValidator(Constants::MERCHANT_SUBMISSION_FORM_TYPE, [])
        );
    }

    public function testGetPostCreatorReturnsAppropriateClass()
    {
        $this->assertInstanceOf(
            'TaylorThomas\WordPress\DekoForms\LenderSubmissionPostCreator',
            Factory::getPostCreator(Constants::LENDER_SUBMISSION_FORM_TYPE, [], [])
        );

        $this->assertInstanceOf(
            'TaylorThomas\WordPress\DekoForms\MerchantSubmissionPostCreator',
            Factory::getPostCreator(Constants::MERCHANT_SUBMISSION_FORM_TYPE, [], [])
        );
    }
}
