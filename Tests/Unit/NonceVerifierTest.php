<?php

namespace TaylorThomas\WordPress\DekoForms\Tests\Unit;

use TaylorThomas\WordPress\DekoForms\Factory;
use TaylorThomas\WordPress\DekoForms\Constants;
use Brain\Monkey\Functions;

class NonceVerifierTestCase extends TestCase
{
  const NONCE = 'nonce';

  const VALID_NONCE_RETURN   = 1;
  const OLD_NONCE_RETURN     = 2;
  const INVALID_NONCE_RETURN = false;

  
  protected function stubWPFunction($nonce, $return)
  {
    Functions\expect('wp_verify_nonce')
    ->once()
    ->with($nonce, Constants::AJAX_ACTION)
    ->andReturn($return);
  }


  public function testGetIsVerfiedReturnsTrueWithValidNonce()
  {
    $verifier = Factory::getNonceVerifier(self::NONCE);
    $this->stubWPFunction(self::NONCE, self::VALID_NONCE_RETURN);
    $this->assertTrue($verifier->getIsVerified());
  }

  public function testGetIsVerfiedReturnsFalseWithNoNonce()
  {
    $verifier = Factory::getNonceVerifier(null);
    $this->stubWPFunction(null, self::INVALID_NONCE_RETURN);
    $this->assertFalse($verifier->getIsVerified());
  }

  public function testGetIsVerfiedReturnsFalseWithOldNonce()
  {
    $verifier = Factory::getNonceVerifier(self::NONCE);
    $this->stubWPFunction(self::NONCE, self::OLD_NONCE_RETURN);
    $this->assertFalse($verifier->getIsVerified());
  }

  public function testGetIsVerfiedReturnsFalseWithInvalidNonce()
  {
    $verifier = Factory::getNonceVerifier(self::NONCE);
    $this->stubWPFunction(self::NONCE, self::INVALID_NONCE_RETURN);
    $this->assertFalse($verifier->getIsVerified());
  }
}
