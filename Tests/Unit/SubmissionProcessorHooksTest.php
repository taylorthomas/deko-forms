<?php

namespace TaylorThomas\WordPress\DekoForms\Tests\Unit;

use TaylorThomas\WordPress\DekoForms\SubmissionProcessorHooks;
use Brain\Monkey\Functions;

class SubmissionProcessorHooksTestCase extends TestCase
{
    const POST        = ['form-type' => 'lender-submission'];
    const COOKIE      = ['key' => 'value'];
    const ENVIRONMENT = 'test';
    const AWS_KEY     = 'key';
    const AWS_SECRET  = 'secret';

    public function notifyException($e)
    {
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     */
    public function testRegister()
    {
        $notifyException = [$this, 'notifyException'];

        $submissionProcessor = \Mockery::mock('\TaylorThomas\WordPress\DekoForms\SubmissionProcessor');

        $factoryMock = \Mockery::mock('alias:\TaylorThomas\WordPress\DekoForms\SubmissionProcessorFactory');
        $factoryMock->shouldReceive('getInstance')->with(
            self::POST,
            self::COOKIE,
            self::ENVIRONMENT,
            self::AWS_KEY,
            self::AWS_SECRET,
            $notifyException
        )->andReturn($submissionProcessor);

        $hooks = new SubmissionProcessorHooks(
            self::POST,
            self::COOKIE,
            self::ENVIRONMENT,
            self::AWS_KEY,
            self::AWS_SECRET,
            $notifyException
        );

        $hooks->register();

        $this->assertTrue(has_action('wp_ajax_nopriv_process_deko_form_submission', [$hooks, 'processSubmission']));
        $this->assertTrue(has_action('wp_ajax_process_deko_form_submission', [$hooks, 'processSubmission']));
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     */
    public function testProcessSubmission()
    {
        $notifyException = [$this, 'notifyException'];

        $result = \Mockery::mock('\TaylorThomas\WordPress\DekoForms\SubmissionResultInterface');
        $result->shouldReceive('getResponseCode')->once()->andReturn(200);
        $result->shouldReceive('getData')->once()->andReturn(['key' => 'value']);

        $submissionProcessor = \Mockery::mock('\TaylorThomas\WordPress\DekoForms\SubmissionProcessor');
        $submissionProcessor->shouldReceive('process')->andReturn($result);

        $factoryMock = \Mockery::mock('alias:\TaylorThomas\WordPress\DekoForms\SubmissionProcessorFactory');
        $factoryMock->shouldReceive('getInstance')->with(
            self::POST,
            self::COOKIE,
            self::ENVIRONMENT,
            self::AWS_KEY,
            self::AWS_SECRET,
            $notifyException
        )->andReturn($submissionProcessor);

        Functions\expect('wp_send_json')
            ->once()
            ->with(['key' => 'value'], 200);

        $hooks = new SubmissionProcessorHooks(
            self::POST,
            self::COOKIE,
            self::ENVIRONMENT,
            self::AWS_KEY,
            self::AWS_SECRET,
            $notifyException
        );

        $this->assertNull($hooks->processSubmission());
    }
}
