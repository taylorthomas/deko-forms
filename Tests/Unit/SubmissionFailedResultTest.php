<?php

namespace TaylorThomas\WordPress\DekoForms\Tests\Unit;

use TaylorThomas\WordPress\DekoForms\Factory;

class SubmissionFailedResultTestCase extends TestCase
{
    protected function getSubmissionFailedResult()
    {
        return Factory::getSubmissionFailedResult();
    }

    public function testGetResponseCode()
    {
      $this->assertEquals(500, $this->getSubmissionFailedResult()->getResponseCode());
    }

    public function testGetData()
    {
        $this->assertEquals([
            'message' => 'There was an unexpected error.'
        ], $this->getSubmissionFailedResult()->getData());
    }
}
