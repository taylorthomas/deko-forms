<?php

namespace TaylorThomas\WordPress\DekoForms\Tests\Unit;

use TaylorThomas\WordPress\DekoForms\Factory;
use TaylorThomas\WordPress\DekoForms\Constants;

class MerchantSubmissionValidatorTestCase extends TestCase
{
  const VALID_DATA = [
    'first-name'      => 'Stephen',
    'last-name'       => 'Merchant',
    'email-address'   => 'steve@example.com',
    'phone-number'    => '0800 123 456',
    'company-name'    => 'Loans 4U RUS',
    'website-address' => 'www.example.com',
    'turnover'        => '1 million',
    'opt-in'          => '1',
    'acceptance'      => '1'
  ];

  private function getValidator($data = self::VALID_DATA)
  {
    return Factory::getValidator(Constants::MERCHANT_SUBMISSION_FORM_TYPE, $data);
  }

  public function testGetIsValidReturnsTrueWithValidData()
  {
    $validator = $this->getValidator();
    $this->assertTrue($validator->getIsValid());
  }

  public function testGetErrorsReturnsNullWhenValid()
  {
    $validator = $this->getValidator();
    $validator->getIsValid();
    $this->assertEmpty($validator->getErrors());
  }

  public function testGetIsValidReturnsFalseWithInvalidData()
  {
    $data = array_merge([], self::VALID_DATA, ['website-address' => 'ftp://invalid.com']);
    $validator = $this->getValidator($data);
    $this->assertFalse($validator->getIsValid());
  }
}
