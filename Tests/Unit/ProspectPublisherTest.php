<?php

namespace TaylorThomas\WordPress\DekoForms\Tests\Unit;

use TaylorThomas\WordPress\DekoForms\Factory;

class ProspectPublisherTestCase extends TestCase
{
    const POST        = ['field' => 'value'];
    const COOKIE      = ['key' => 'value'];
    const ENVIRONMENT = 'test';
    const AWS_KEY     = 'key';
    const AWS_SECRET  = 'secret';


    /**
     * @runInSeparateProcess
     */
    public function testPublish()
    {
        $pay4LaterProspectPublisherMock = \Mockery::mock('\Pay4Later\Website\ProspectPublisher');
        $pay4LaterProspectPublisherMock->shouldReceive('publish')->once()->andReturnNull();

        $factoryMock = \Mockery::mock('alias:\Pay4Later\Website\Factory');
        $factoryMock->shouldReceive('getProspectPublisher')->with(
            self::POST,
            self::COOKIE,
            self::ENVIRONMENT,
            self::AWS_KEY,
            self::AWS_SECRET
        )->andReturn($pay4LaterProspectPublisherMock);


        $publisher = Factory::getProspectPublisher(
            self::POST,
            self::COOKIE,
            self::ENVIRONMENT,
            self::AWS_KEY,
            self::AWS_SECRET
        );

        $this->assertFalse($publisher->publish());
    }
}
