<?php

namespace TaylorThomas\WordPress\DekoForms\Tests\Unit;

use TaylorThomas\WordPress\DekoForms\Factory;
use TaylorThomas\WordPress\DekoForms\Constants;

class LenderSubmissionValidatorTestCase extends TestCase
{
  const VALID_DATA = [
    'first-name'    => 'Lenny',
    'last-name'     => 'Lender',
    'email-address' => 'lenny@example.com',
    'phone-number'  => '0800 123 456',
    'company-name'  => 'Loans 4U RUS',
    'comments'      => 'I want in',
    'opt-in'        => '1',
    'acceptance'    => '1'
  ];

  private function getValidator($data = self::VALID_DATA)
  {
    return Factory::getValidator(Constants::LENDER_SUBMISSION_FORM_TYPE, $data);
  }

  public function testGetIsValidReturnsTrueWithValidData()
  {
    $validator = $this->getValidator();
    $this->assertTrue($validator->getIsValid());
  }

  public function testGetErrorsReturnsNullWhenValid()
  {
    $validator = $this->getValidator();
    $validator->getIsValid();
    $this->assertEmpty($validator->getErrors());
  }

  public function testGetIsValidReturnsFalseWithInvalidData()
  {
    $data = array_merge([], self::VALID_DATA, ['email-address' => 'missing-at.com']);
    $validator = $this->getValidator($data);
    $this->assertFalse($validator->getIsValid());
  }
}
