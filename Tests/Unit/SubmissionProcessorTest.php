<?php

namespace TaylorThomas\WordPress\DekoForms\Tests\Unit;

use TaylorThomas\WordPress\DekoForms\SubmissionProcessor;
use TaylorThomas\WordPress\DekoForms\Factory;
use \Mockery;

class SubmissionProcessorTestCase extends TestCase
{
    const ERRORS    = ['email-address' => 'is not valid'];
    const POST_TYPE = 'lender-submission';


    protected function getMockedValidator($response)
    {
        $validator = \Mockery::mock('TaylorThomas\WordPress\DekoForms\ValidatorInterface');
        $validator->shouldReceive('getIsValid')->once()->andReturn($response);
        if (!$response) {
            $validator->shouldReceive('getErrors')->once()->andReturn(self::ERRORS);
        }
        return $validator;
    }

    protected function getMockedVerifier($response)
    {
        $verifier = \Mockery::mock('TaylorThomas\WordPress\DekoForms\NonceVerifier');
        $verifier->shouldReceive('getIsVerified')->once()->andReturn($response);
        return $verifier;
    }



    public function testProcessWhenNonceVerificationFails()
    {
        $notifier = \Mockery::mock();
        $notifyException = [$notifier, 'notifyException'];

        $processor = new SubmissionProcessor(
            $this->getMockedVerifier(false),
            Factory::getValidator(self::POST_TYPE, []),
            Factory::getPostCreator(self::POST_TYPE, [], []),
            Factory::getProspectPublisher([], [], '', '', ''),
            Factory::getConfirmation(self::POST_TYPE, []),
            $notifyException
        );

        $result = $processor->process();

        $this->assertEquals(500, $result->getResponseCode());
        $this->assertEquals(['message' => 'There was an unexpected error.'], $result->getData());
    }


    public function testProcessWhenValdationFails()
    {
        $notifier = \Mockery::mock();
        $notifyException = [$notifier, 'notifyException'];

        $processor = new SubmissionProcessor(
            $this->getMockedVerifier(true),
            $this->getMockedValidator(false),
            Factory::getPostCreator(self::POST_TYPE, [], []),
            Factory::getProspectPublisher([], [], '', '', ''),
            Factory::getConfirmation(self::POST_TYPE, []),
            $notifyException
        );

        $result = $processor->process();

        $this->assertEquals(422, $result->getResponseCode());
        $this->assertEquals(['errors' => self::ERRORS], $result->getData());
    }


    public function testProcessWhenCreateProspectFails()
    {
        $exception = new \Exception();

        $notifier = \Mockery::mock();
        $notifier->shouldReceive('notifyException')->once()->with($exception);
        $notifyException = [$notifier, 'notifyException'];

        $prospectPublisher = \Mockery::mock('TaylorThomas\WordPress\DekoForms\ProspectPublisher');
        $prospectPublisher->shouldReceive('publish')->andThrow($exception);

        $postCreator = \Mockery::mock('TaylorThomas\WordPress\DekoForms\LenderSubmissionPostCreator');
        $postCreator->shouldReceive('create')->once()->with($exception);

        $processor = new SubmissionProcessor(
            $this->getMockedVerifier(true),
            $this->getMockedValidator(true),
            $postCreator,
            $prospectPublisher,
            Factory::getConfirmation(self::POST_TYPE, []),
            $notifyException
        );

        $result = $processor->process();

        $this->assertEquals(200, $result->getResponseCode());
        $this->assertEquals(['message' => 'Thanks - We’ll be in touch as soon as we can.'], $result->getData());
    }


    public function testProcessWhenCreatePostFails()
    {
        $exception = new \Exception();

        $notifier = \Mockery::mock();
        $notifier->shouldReceive('notifyException')->once()->with($exception);
        $notifyException = [$notifier, 'notifyException'];

        $prospectPublisher = \Mockery::mock('TaylorThomas\WordPress\DekoForms\ProspectPublisher');
        $prospectPublisher->shouldReceive('publish')->once();

        $postCreator = \Mockery::mock('TaylorThomas\WordPress\DekoForms\LenderSubmissionPostCreator');
        $postCreator->shouldReceive('create')->once()->with(null)->andThrow($exception);

        $processor = new SubmissionProcessor(
            $this->getMockedVerifier(true),
            $this->getMockedValidator(true),
            $postCreator,
            $prospectPublisher,
            Factory::getConfirmation(self::POST_TYPE, []),
            $notifyException
        );

        $result = $processor->process();

        $this->assertEquals(200, $result->getResponseCode());
        $this->assertEquals(['message' => 'Thanks - We’ll be in touch as soon as we can.'], $result->getData());
    }


    public function testProcessWhenCreatePostAndCreateProspectSucceed()
    {
        $notifier = \Mockery::mock();
        $notifyException = [$notifier, 'notifyException'];

        $prospectPublisher = \Mockery::mock('TaylorThomas\WordPress\DekoForms\ProspectPublisher');
        $prospectPublisher->shouldReceive('publish')->once();

        $postCreator = \Mockery::mock('TaylorThomas\WordPress\DekoForms\LenderSubmissionPostCreator');
        $postCreator->shouldReceive('create')->once()->with(null);

        $processor = new SubmissionProcessor(
            $this->getMockedVerifier(true),
            $this->getMockedValidator(true),
            $postCreator,
            $prospectPublisher,
            Factory::getConfirmation(self::POST_TYPE, []),
            $notifyException
        );

        $result = $processor->process();

        $this->assertEquals(200, $result->getResponseCode());
        $this->assertEquals(['message' => 'Thanks - We’ll be in touch as soon as we can.'], $result->getData());
    }


    public function testProcessWhenCreatePostAndCreateProspectFail()
    {
        $prospectPublishException = new \Exception();
        $postCreationException    = new \Exception();

        $notifier = \Mockery::mock();
        $notifier->shouldReceive('notifyException')->once()->with($prospectPublishException);
        $notifier->shouldReceive('notifyException')->once()->with($postCreationException);
        $notifyException = [$notifier, 'notifyException'];


        $prospectPublisher = \Mockery::mock('TaylorThomas\WordPress\DekoForms\ProspectPublisher');
        $prospectPublisher->shouldReceive('publish')->andThrow($prospectPublishException);

        $postCreator = \Mockery::mock('TaylorThomas\WordPress\DekoForms\LenderSubmissionPostCreator');
        $postCreator->shouldReceive('create')->once()->andThrow($postCreationException);

        $processor = new SubmissionProcessor(
            $this->getMockedVerifier(true),
            $this->getMockedValidator(true),
            $postCreator,
            $prospectPublisher,
            Factory::getConfirmation(self::POST_TYPE, []),
            $notifyException
        );

        $result = $processor->process();

        $this->assertEquals(500, $result->getResponseCode());
        $this->assertEquals(['message' => 'There was an unexpected error.'], $result->getData());
    }
}
