<?php

namespace TaylorThomas\WordPress\DekoForms\Tests\Unit;

use TaylorThomas\WordPress\DekoForms\Factory;
use Brain\Monkey\Functions;
use TaylorThomas\WordPress\DekoForms\Constants;

class MerchantSubmissionPostCreatorTestCase extends TestCase
{
    const POST = [
        'first-name'      => 'Stephen',
        'last-name'       => 'merchant',
        'email-address'   => 'steve@example.com',
        'phone-number'    => '0800 000 000',
        'company-name'    => 'Lenny Inc.',
        'website-address' => 'http://www.example.com',
        'turnover'        => '1 million',
        'opt-in'          => '1',
        'other-field'     => 'Whatever'
    ];

    const COOKIE = [
        'visitor_id73452' => '123'
    ];

    const POST_ID = 5;

    public function testCreate()
    {
        $creator= Factory::getPostCreator(Constants::MERCHANT_SUBMISSION_FORM_TYPE, self::POST, self::COOKIE);

        Functions\expect('wp_insert_post')
            ->once()
            ->with(['post_type' => 'deko_merchant_sub'], true)
            ->andReturn(self::POST_ID);

        Functions\expect('update_field')
            ->once()
            ->with('merchant_first_name', self::POST['first-name'], self::POST_ID)
            ->ordered();

        Functions\expect('update_field')
            ->once()
            ->with('merchant_last_name', self::POST['last-name'], self::POST_ID)
            ->ordered();

        Functions\expect('update_field')
            ->once()
            ->with('merchant_email_address', self::POST['email-address'], self::POST_ID)
            ->ordered();

        Functions\expect('update_field')
            ->once()
            ->with('merchant_phone_number', self::POST['phone-number'], self::POST_ID)
            ->ordered();

        Functions\expect('update_field')
            ->once()
            ->with('merchant_company_name', self::POST['company-name'], self::POST_ID)
            ->ordered();

        Functions\expect('update_field')
            ->once()
            ->with('merchant_website_address', self::POST['website-address'], self::POST_ID)
            ->ordered();

        Functions\expect('update_field')
            ->once()
            ->with('merchant_turnover', self::POST['turnover'], self::POST_ID)
            ->ordered();

        Functions\expect('update_field')
            ->once()
            ->with('merchant_opt_in', self::POST['opt-in'], self::POST_ID)
            ->ordered();

        Functions\expect('update_field')
            ->once()
            ->with('merchant_visitor_id', self::COOKIE['visitor_id73452'], self::POST_ID)
            ->ordered();

        Functions\expect('update_field')
            ->never()
            ->with('merchant_other_field', self::POST['other-field'], self::POST_ID);

        $this->assertTrue($creator->create());
    }
}
