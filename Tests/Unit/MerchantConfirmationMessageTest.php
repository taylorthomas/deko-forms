<?php

namespace TaylorThomas\WordPress\DekoForms\Tests\Unit;

use TaylorThomas\WordPress\DekoForms\Factory;
use TaylorThomas\WordPress\DekoForms\Constants;

class MerchantConfirmationTestCase extends TestCase
{
    public function testGetMessageReturnsRelevantMessage()
    {
      $confirmation= Factory::getConfirmation(
          Constants::MERCHANT_SUBMISSION_FORM_TYPE,
          ['turnover' => Constants::TURNOVER_OPTIONS[0]]
      );

      $this->assertEquals('Thank you for your enquiry, one of our sales teams will be in touch with you shortly.', $confirmation->getMessage());

      $confirmation= Factory::getConfirmation(
          Constants::MERCHANT_SUBMISSION_FORM_TYPE,
          ['turnover' => Constants::TURNOVER_OPTIONS[1]]
      );

      $this->assertEquals('Thank you for your enquiry, one of our sales teams will be in touch with you shortly.', $confirmation->getMessage());
    }
}
