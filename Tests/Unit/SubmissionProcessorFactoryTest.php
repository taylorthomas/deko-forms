<?php

namespace TaylorThomas\WordPress\DekoForms\Tests\Unit;

use TaylorThomas\WordPress\DekoForms\SubmissionProcessorFactory;

class SubmissionProcessorFactoryTestCase extends TestCase
{
    const FORM_TYPE   = 'lender-submission';
    const POST        = ['form-type' => self::FORM_TYPE];
    const COOKIE      = ['key' => 'value'];
    const ENVIRONMENT = 'test';
    const AWS_KEY     = 'key';
    const AWS_SECRET  = 'secret';

    public function testGetInstanceReturnsASubmissionProcessor()
    {
      $factory = SubmissionProcessorFactory::getInstance(
          self::POST,
          self::COOKIE,
          self::ENVIRONMENT,
          self::AWS_KEY,
          self::AWS_SECRET,
          function ($e) {
          }
      );

      $this->assertInstanceOf(
          'TaylorThomas\WordPress\DekoForms\SubmissionProcessor',
          $factory
      );

      $this->markTestIncomplete(
          'Not testing that SubmissionProcessor is instantiated with the correct arguments!'
      );
    }
}
