<?php

namespace TaylorThomas\WordPress\DekoForms\Tests\Unit;

use TaylorThomas\WordPress\DekoForms\Factory;
use TaylorThomas\WordPress\DekoForms\Constants;

class LenderConfirmationTestCase extends TestCase
{
    public function testGetMessage()
    {
        $confirmation= Factory::getConfirmation(Constants::LENDER_SUBMISSION_FORM_TYPE, []);
        $this->assertEquals('Thanks - We’ll be in touch as soon as we can.', $confirmation->getMessage());
    }
}
