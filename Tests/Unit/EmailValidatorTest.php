<?php

namespace TaylorThomas\WordPress\DekoForms\Tests\Unit;

use TaylorThomas\WordPress\DekoForms\Factory;

class EmailValidatorTestCase extends TestCase
{
  /**
   * @dataProvider errorProvider
   */
  public function testGetError($emailAddress, $expected)
  {
    $error = Factory::getEmailValidator($emailAddress)->getError();
    $this->assertEquals($expected, $error);
  }

  public function errorProvider()
  {
    return [
      'invalid'     => ['invalid.email.com',     'is not valid'],
      'blacklisted' => ['blacklisted@gmail.com', 'must be a business address'],
      'valid'       => ['valid@example.com',     null]
    ];
  }
}
