<?php

namespace TaylorThomas\WordPress\DekoForms\Tests\Unit;

use TaylorThomas\WordPress\DekoForms\UrlValidator;

class UrlValidatorTestCase extends TestCase
{
  /**
   * @dataProvider errorProvider
   */
  public function testGetError($url, $expected)
  {
    $error = (new UrlValidator($url))->getError();
    $this->assertEquals($expected, $error);
  }

  public function errorProvider()
  {
    return [
      ['invalid',                 'is not valid'],
      ['ftp://invalid.com',       'is not valid'],
      ['invalid.com/folder',      'is not valid'],
      ['invalid.com?querystring', 'is not valid'],
      ['http://http://valid.com', 'is not valid'],
      ['http://valid.com',        null],
      ['valid.com',               null]
    ];
  }
}
