<?php

namespace TaylorThomas\WordPress\DekoForms\Tests\Unit;

use TaylorThomas\WordPress\DekoForms\Factory;
use TaylorThomas\WordPress\DekoForms\Constants;
use Brain\Monkey\Functions;

class LenderSubmissionPostCreatorTestCase extends TestCase
{
    const POST = [
        'first-name'      => 'Lenny',
        'last-name'       => 'Lender',
        'email-address'   => 'lenny@example.com',
        'phone-number'    => '0800 000 000',
        'company-name'    => 'Lenny Inc.',
        'comments'        => 'Some comments',
        'opt-in'          => '1',
        'other-field'     => 'Whatever'
    ];

    const COOKIE = [
        'visitor_id73452' => '123'
    ];

    const POST_ID = 5;

    public function testCreate()
    {
        $creator= Factory::getPostCreator(Constants::LENDER_SUBMISSION_FORM_TYPE, self::POST, self::COOKIE);

        Functions\expect('wp_insert_post')
            ->once()
            ->with(['post_type' => 'deko_lender_sub'], true)
            ->andReturn(self::POST_ID);

        Functions\expect('update_field')
            ->once()
            ->with('lender_first_name', self::POST['first-name'], self::POST_ID)
            ->ordered();

        Functions\expect('update_field')
            ->once()
            ->with('lender_last_name', self::POST['last-name'], self::POST_ID)
            ->ordered();

        Functions\expect('update_field')
            ->once()
            ->with('lender_email_address', self::POST['email-address'], self::POST_ID)
            ->ordered();

        Functions\expect('update_field')
            ->once()
            ->with('lender_phone_number', self::POST['phone-number'], self::POST_ID)
            ->ordered();

        Functions\expect('update_field')
            ->once()
            ->with('lender_company_name', self::POST['company-name'], self::POST_ID)
            ->ordered();

        Functions\expect('update_field')
            ->once()
            ->with('lender_comments', self::POST['comments'], self::POST_ID)
            ->ordered();

        Functions\expect('update_field')
            ->once()
            ->with('lender_opt_in', self::POST['opt-in'], self::POST_ID)
            ->ordered();

        Functions\expect('update_field')
            ->once()
            ->with('lender_visitor_id', self::COOKIE['visitor_id73452'], self::POST_ID)
            ->ordered();

        Functions\expect('update_field')
            ->never()
            ->with('lender_other_field', self::POST['other-field'], self::POST_ID);

        $this->assertTrue($creator->create());
    }
}
