<?php

namespace TaylorThomas\WordPress\DekoForms;

abstract class AbstractPostCreator implements PostCreatorInterface
{
    const RAISE_WP_ERROR_ON_FAILURE = true;

    /** @var array $ */
    protected $post;

    /** @var array $ */
    protected $cookie;

    /**
     * @param array $post
     * @param array $cookie
     */
    public function __construct(array $post, array $cookie)
    {
        $this->post   = $post;
        $this->cookie = $cookie;
    }

    /**
     * @return bool
     */
    public function create()
    {
        $postId = wp_insert_post([
            'post_type' => $this->getPostType()
        ], self::RAISE_WP_ERROR_ON_FAILURE);

        foreach ($this->getKeys() as $postKey) {
            update_field($this->getAcfKey($postKey), $this->post[$postKey], $postId);
        }

        update_field($this->getAcfKey('visitor-id'), $this->cookie['visitor_id73452'], $postId);

        return true;
    }

    /**
     * @return string
     */
    protected function getPostType()
    {
        return 'deko_' . $this->getType() . '_sub';
    }

    /**
     * @param string $postKey
     * @return bool
     */
    protected function getAcfKey(string $postKey)
    {
        return $this->getType() . '_' . str_replace('-', '_', $postKey);
    }

    /**
     * @return array
     */
    abstract protected function getKeys();

    /**
     * @return string
     */
    abstract protected function getType();
}
