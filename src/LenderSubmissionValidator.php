<?php

namespace TaylorThomas\WordPress\DekoForms;

class LenderSubmissionValidator extends AbstractValidator
{
    /**
     * @return void
     */
    protected function validate()
    {
        parent::validate();
        $this->validateEmailAddress(Constants::EMAIL_ADDRESS);
    }

    /**
     * @return array
     */
    protected function getMandatories()
    {
        return [
            Constants::FIRST_NAME,
            Constants::LAST_NAME,
            Constants::EMAIL_ADDRESS,
            Constants::PHONE_NUMBER,
            Constants::COMPANY_NAME,
            Constants::COMMENTS,
            Constants::ACCEPTANCE
        ];
    }
}
