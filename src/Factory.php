<?php

namespace TaylorThomas\WordPress\DekoForms;

class Factory
{
    public static function getEmailValidator($emailAddress) : EmailValidator
    {
        return new EmailValidator($emailAddress);
    }
    
    public static function getProspectPublisher(
        array $post,
        array $cookie,
        string $environment,
        string $awsKey,
        string $awsSecret
    ) : ProspectPublisher {
        return new ProspectPublisher(
            $post,
            $cookie,
            $environment,
            $awsKey,
            $awsSecret
        );
    }

    public static function getSubmissionSucceededResult(string $message) : SubmissionSucceededResult
    {
        return new SubmissionSucceededResult($message);
    }

    public static function getSubmissionFailedResult() : SubmissionFailedResult
    {
        return new SubmissionFailedResult();
    }

    public static function getSubmissionInvalidResult(array $errors) : SubmissionInvalidResult
    {
        return new SubmissionInvalidResult($errors);
    }

    public static function getNonceVerifier(?string $nonce) : NonceVerifier
    {
        return new NonceVerifier($nonce);
    }

    public static function getConfirmation(string $type, array $post) : AbstractConfirmation
    {
        switch ($type) {
            case Constants::LENDER_SUBMISSION_FORM_TYPE:
                return self::getLenderConfirmation($post);
            case Constants::MERCHANT_SUBMISSION_FORM_TYPE:
                return self::getMerchantConfirmation($post);
        }
    }

    public static function getValidator(string $type, array $post) : AbstractValidator
    {
        switch ($type) {
            case Constants::LENDER_SUBMISSION_FORM_TYPE:
                return self::getLenderSubmissionValidator($post);
            case Constants::MERCHANT_SUBMISSION_FORM_TYPE:
                return self::getMerchantSubmissionValidator($post);
        }
    }

    public static function getPostCreator(string $type, array $post, array $cookie) : AbstractPostCreator
    {
        switch ($type) {
            case Constants::LENDER_SUBMISSION_FORM_TYPE:
                return self::getLenderSubmissionPostCreator($post, $cookie);
            case Constants::MERCHANT_SUBMISSION_FORM_TYPE:
                return self::getMerchantSubmissionPostCreator($post, $cookie);
        }
    }

    protected static function getLenderConfirmation(array $post) : LenderConfirmation
    {
        return new LenderConfirmation($post);
    }

    protected static function getMerchantConfirmation(array $post) : MerchantConfirmation
    {
        return new MerchantConfirmation($post);
    }

    protected static function getLenderSubmissionPostCreator(array $post, array $cookie) : LenderSubmissionPostCreator
    {
        return new LenderSubmissionPostCreator($post, $cookie);
    }

    protected static function getMerchantSubmissionPostCreator(array $post, array $cookie) : MerchantSubmissionPostCreator
    {
        return new MerchantSubmissionPostCreator($post, $cookie);
    }

    protected static function getLenderSubmissionValidator(array $data) : LenderSubmissionValidator
    {
        return new LenderSubmissionValidator($data);
    }

    protected static function getMerchantSubmissionValidator(array $data) : MerchantSubmissionValidator
    {
        return new MerchantSubmissionValidator($data);
    }
}
