<?php

namespace TaylorThomas\WordPress\DekoForms;

class SubmissionProcessorHooks
{
    /** @var array $ */
    protected $post;

    /** @var array $ */
    protected $cookie;

    /** @var string $ */
    protected $environment;

    /** @var string $ */
    protected $awsKey;

    /** @var string $ */
    protected $awsSecret;

    /** @var callable $ */
    protected $notifyException;

    /**
     * @param array $post
     * @param array $cookie
     * @param string $environment
     * @param string $awsKey
     * @param string $awsSecret
     */
    public function __construct(
        array $post,
        array $cookie,
        string $environment,
        string $awsKey,
        string $awsSecret,
        callable $notifyException
    ) {
        $this->post              = $post;
        $this->cookie            = $cookie;
        $this->environment       = $environment;
        $this->awsKey            = $awsKey;
        $this->awsSecret         = $awsSecret;
        $this->notifyException   = $notifyException;
    }

    /**
     * @return void
     */
    public function register()
    {
        $method = [$this, 'processSubmission'];
        add_action('wp_ajax_nopriv_' . Constants::AJAX_ACTION, $method);
        add_action('wp_ajax_'        . Constants::AJAX_ACTION, $method);
    }

    /**
     * @return void
     */
    public function processSubmission()
    {
        $result = $this->getSubmissionProcessor()->process();
        wp_send_json($result->getData(), $result->getResponseCode());
    }

    /**
     * @return SubmissionProcessor
     */
    public function getSubmissionProcessor()
    {
        return SubmissionProcessorFactory::getInstance(
            $this->post,
            $this->cookie,
            $this->environment,
            $this->awsKey,
            $this->awsSecret,
            $this->notifyException
        );
    }
}
