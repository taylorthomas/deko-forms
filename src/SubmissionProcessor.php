<?php

namespace TaylorThomas\WordPress\DekoForms;

class SubmissionProcessor
{
    /** @var NonceVerifier $nonceVerifier */
    protected $nonceVerifier;

    /** @var ValidatorInterface $validator */
    protected $validator;

    /** @var PostCreatorInterface $postCreator */
    protected $postCreator;

    /** @var ProspectPublisherInterface $prospectPublisher */
    protected $prospectPublisher;

    /** @var ConfirmationInterface $confirmation */
    protected $confirmation;

    /** @var string $prospectCreationException */
    protected $prospectCreationException;

    /** @var callable $ */
    protected $notifyException;

    /**
     * @param NonceVerifier $nonceVerifier
     * @param ValidatorInterface $validator
     * @param PostCreatorInterface $postCreator
     * @param ProspectPublisher $prospectPublisher
     * @param ConfirmationInterface $confirmation
     * @param callable $notifyException
     */
    public function __construct(
        NonceVerifier $nonceVerifier,
        ValidatorInterface $validator,
        PostCreatorInterface $postCreator,
        ProspectPublisher $prospectPublisher,
        ConfirmationInterface $confirmation,
        callable $notifyException
    ) {
        $this->nonceVerifier     = $nonceVerifier;
        $this->validator         = $validator;
        $this->postCreator       = $postCreator;
        $this->prospectPublisher = $prospectPublisher;
        $this->confirmation      = $confirmation;
        $this->notifyException   = $notifyException;
    }

    /**
     * @return SubmissionResultInterface
     */
    public function process()
    {
        if (!$this->nonceVerifier->getIsVerified()) {
            return Factory::getSubmissionFailedResult();
        } if ($this->validator->getIsValid()) {
            $wasProspectCreated = $this->createProspect();
            $wasPostCreated     = $this->createPost();
            if (!$wasProspectCreated && !$wasPostCreated) {
                return Factory::getSubmissionFailedResult();
            }
            return Factory::getSubmissionSucceededResult($this->confirmation->getMessage());
        } else {
            return Factory::getSubmissionInvalidResult($this->validator->getErrors());
        }
    }

    /**
     * @return bool
     */
    protected function createProspect()
    {
        try {
            $this->prospectPublisher->publish();
            return true;
        } catch (\Exception $e) {
            $this->prospectCreationException = $e;
            call_user_func($this->notifyException, $e);
            return false;
        }
    }

    /**
     * @return bool
     */
    protected function createPost()
    {
        try {
            $this->postCreator->create($this->prospectCreationException);
            return true;
        } catch (\Exception $e) {
            call_user_func($this->notifyException, $e);
            return false;
        }
    }
}
