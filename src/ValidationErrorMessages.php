<?php

namespace TaylorThomas\WordPress\DekoForms;

class ValidationErrorMessages
{
    const BLANK_MSG   = 'must be completed';
    const INVALID_MSG = 'is not valid';
}
