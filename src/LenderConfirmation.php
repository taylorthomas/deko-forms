<?php

namespace TaylorThomas\WordPress\DekoForms;

class LenderConfirmation extends AbstractConfirmation
{
    /**
     * @return string
     */
    public function getMessage()
    {
        return 'Thanks - We’ll be in touch as soon as we can.';
    }
}
