<?php

namespace TaylorThomas\WordPress\DekoForms;

class UrlValidator
{
    const REG_EXP = '/^((http|https):\/\/)?[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}$/ix';

    /** @var string $ */
    protected $url;

    /**
     * @param string $url
     */
    public function __construct(string $url)
    {
        $this->url = $url;
    }

    /**
     * @return string
     */
    public function getError()
    {
        if (preg_match(self::REG_EXP, $this->url) !== 1) {
            return ValidationErrorMessages::INVALID_MSG;
        }
    }
}
