<?php

namespace TaylorThomas\WordPress\DekoForms;

class LenderSubmissionPostCreator extends AbstractPostCreator
{
    /**
     * @return array
     */
    protected function getKeys()
    {
        return [
            Constants::FIRST_NAME,
            Constants::LAST_NAME,
            Constants::EMAIL_ADDRESS,
            Constants::PHONE_NUMBER,
            Constants::COMPANY_NAME,
            Constants::COMMENTS,
            Constants::OPT_IN
        ];
    }

    /**
     * @return string
     */
    protected function getType()
    {
        return 'lender';
    }
}
