<?php

namespace TaylorThomas\WordPress\DekoForms;

class NonceVerifier
{
    /** @var string $nonce */
    protected $nonce;

    /**
     * @param string $nonce
     */
    public function __construct(
        ?string $nonce
    ) {
        $this->nonce = $nonce;
    }

    /**
     * @return bool
     */
    public function getIsVerified()
    {
        return wp_verify_nonce($this->nonce, Constants::AJAX_ACTION) === 1;
    }
}
