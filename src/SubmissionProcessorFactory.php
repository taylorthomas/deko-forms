<?php

namespace TaylorThomas\WordPress\DekoForms;

class SubmissionProcessorFactory
{
    /**
     * @return SubmissionProcessor
     */
    public static function getInstance(
        array $post,
        array $cookie,
        string $environment,
        string $awsKey,
        string $awsSecret,
        callable $notifyException
    ) : SubmissionProcessor {

        $formType = $post['form-type'];

        $nonceVerifier = Factory::getNonceVerifier($post[Constants::NONCE_FIELD_NAME] ?? null);

        $validator = Factory::getValidator($formType, $post);

        $postCreator = Factory::getPostCreator($formType, $post, $cookie);

        $prospectPublisher = Factory::getProspectPublisher(
            $post,
            $cookie,
            $environment,
            $awsKey,
            $awsSecret
        );

        $confirmation = Factory::getConfirmation($formType, $post, $cookie);

        return new SubmissionProcessor(
            $nonceVerifier,
            $validator,
            $postCreator,
            $prospectPublisher,
            $confirmation,
            $notifyException
        );
    }
}
