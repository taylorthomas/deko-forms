<?php

namespace TaylorThomas\WordPress\DekoForms;

interface ValidatorInterface
{
    /**
     * @return bool
     */
    public function getIsValid();

    /**
     * @return array
     */
    public function getErrors();
}
