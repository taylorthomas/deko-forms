<?php

namespace TaylorThomas\WordPress\DekoForms;

class MerchantSubmissionValidator extends AbstractValidator
{
    /**
     * @return void
     */
    protected function validate()
    {
        parent::validate();
        $this->validateEmailAddress(Constants::EMAIL_ADDRESS);
        $this->validateWebsiteAddress();
    }

    /**
     * @return array
     */
    protected function getMandatories()
    {
        return [
            Constants::FIRST_NAME,
            Constants::LAST_NAME,
            Constants::EMAIL_ADDRESS,
            Constants::PHONE_NUMBER,
            Constants::COMPANY_NAME,
            Constants::WEBSITE_ADDRESS,
            Constants::TURNOVER,
            Constants::ACCEPTANCE
        ];
    }

    /**
     * @return void
     */
    protected function validateWebsiteAddress()
    {
        if (!isset($this->errors[Constants::WEBSITE_ADDRESS])) {
            $error = (new UrlValidator($this->data[Constants::WEBSITE_ADDRESS]))->getError();
            if ($error) {
                $this->errors[Constants::WEBSITE_ADDRESS] = $error;
            }
        }
    }
}
