<?php

namespace TaylorThomas\WordPress\DekoForms;

class SubmissionInvalidResult implements SubmissionResultInterface
{
    /** @var array $errors */
    protected $errors;

    /**
     * @param array $errors
     */
    public function __construct(array $errors)
    {
        $this->errors = $errors;
    }

    /**
     * @return int
     */
    public function getResponseCode()
    {
        return 422;
    }

    /**
     * @return array
     */
    public function getData()
    {
        return [
            'errors' => $this->errors
        ];
    }
}
