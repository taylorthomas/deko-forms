<?php

namespace TaylorThomas\WordPress\DekoForms;

abstract class AbstractConfirmation implements ConfirmationInterface
{
    /** @var array $post */
    protected $post;

    /**
     * @param array $post
     */
    public function __construct(array $post)
    {
        $this->post = $post;
    }
}
