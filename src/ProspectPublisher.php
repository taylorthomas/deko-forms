<?php

namespace TaylorThomas\WordPress\DekoForms;

class ProspectPublisher
{
    /** @var Pay4Later\Website\ProspectPublisher $prospectPublisher */
    protected $prospectPublisher;

    /**
     * @param array $post
     * @param array $cookie
     * @param string $environment
     * @param string $awsKey
     * @param string $awsSecret
     */
    public function __construct(
        array $post,
        array $cookie,
        string $environment,
        string $awsKey,
        string $awsSecret
    ) {
        $this->prospectPublisher = \Pay4Later\Website\Factory::getProspectPublisher(
            $post,
            $cookie,
            $environment,
            $awsKey,
            $awsSecret
        );
    }

    /**
     * @return bool
     */
    public function publish()
    {
        return !!$this->prospectPublisher->publish();
    }
}
