<?php

namespace TaylorThomas\WordPress\DekoForms;

class SubmissionFailedResult implements SubmissionResultInterface
{
    /**
     * @return int
     */
    public function getResponseCode()
    {
        return 500;
    }

    /**
     * @return array
     */
    public function getData()
    {
        return [
            'message' => 'There was an unexpected error.'
        ];
    }
}
