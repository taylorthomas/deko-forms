<?php

namespace TaylorThomas\WordPress\DekoForms;

class MerchantSubmissionPostCreator extends AbstractPostCreator
{
    /**
     * @return array
     */
    protected function getKeys()
    {
        return [
            Constants::FIRST_NAME,
            Constants::LAST_NAME,
            Constants::EMAIL_ADDRESS,
            Constants::PHONE_NUMBER,
            Constants::COMPANY_NAME,
            Constants::WEBSITE_ADDRESS,
            Constants::TURNOVER,
            Constants::OPT_IN
        ];
    }

    /**
     * @return string
     */
    protected function getType()
    {
        return 'merchant';
    }
}
