<?php

namespace TaylorThomas\WordPress\DekoForms;

interface PostCreatorInterface
{
    /**
     * @return bool
     */
    public function create();
}
