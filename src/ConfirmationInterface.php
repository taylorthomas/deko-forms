<?php

namespace TaylorThomas\WordPress\DekoForms;

interface ConfirmationInterface
{
    /**
     * @return string
     */
    public function getMessage();
}
