<?php

namespace TaylorThomas\WordPress\DekoForms;

class Constants
{
    const AJAX_ACTION = 'process_deko_form_submission';

    const LENDER_SUBMISSION_FORM_TYPE   = 'lender-submission';
    const MERCHANT_SUBMISSION_FORM_TYPE = 'merchant-submission';

    const NONCE_FIELD_NAME = 'nonsence';

    const ACCEPTANCE      = 'acceptance';
    const COMMENTS        = 'comments';
    const COMPANY_NAME    = 'company-name';
    const EMAIL_ADDRESS   = 'email-address';
    const FIRST_NAME      = 'first-name';
    const LAST_NAME       = 'last-name';
    const OPT_IN          = 'opt-in';
    const PHONE_NUMBER    = 'phone-number';
    const TURNOVER        = 'turnover';
    const WEBSITE_ADDRESS = 'website-address';

    const TURNOVER_OPTIONS = [
        'Less than £1m',
        '£1m-5m',
        '£5m-50m',
        '£50m+'
    ];
}
