<?php

namespace TaylorThomas\WordPress\DekoForms;

class SubmissionSucceededResult implements SubmissionResultInterface
{
    /** @var string $ */
    protected $message;

    /**
     * @param string $message
     */
    public function __construct(string $message)
    {
        $this->message = $message;
    }

    /**
     * @return int
     */
    public function getResponseCode()
    {
        return 200;
    }

    /**
     * @return array
     */
    public function getData()
    {
        return [
            'message' => $this->message
        ];
    }
}
