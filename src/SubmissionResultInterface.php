<?php

namespace TaylorThomas\WordPress\DekoForms;

interface SubmissionResultInterface
{
    /**
     * @return int
     */
    public function getResponseCode();

    /**
     * @return array
     */
    public function getData();
}
