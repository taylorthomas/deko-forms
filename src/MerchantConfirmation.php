<?php

namespace TaylorThomas\WordPress\DekoForms;

class MerchantConfirmation extends AbstractConfirmation
{
    const SUFFICIENT_TURNOVER_INDEX = 1;
    const INSUFFICIENT_TURNOVER_MSG = 'Thank you for your enquiry, one of our sales teams will be in touch with you shortly.';
    const SUFFICIENT_TURNOVER_MSG   = 'Thank you for your enquiry, one of our sales teams will be in touch with you shortly.';

    /**
     * @return string
     */
    public function getMessage()
    {
        return $this->getTurnoverOptionIndex() < self::SUFFICIENT_TURNOVER_INDEX
            ? self::INSUFFICIENT_TURNOVER_MSG
            : self::SUFFICIENT_TURNOVER_MSG;
    }

    /**
     * @return int
     */
    protected function getTurnoverOptionIndex()
    {
        return array_search($this->post[Constants::TURNOVER], Constants::TURNOVER_OPTIONS, true);
    }
}
