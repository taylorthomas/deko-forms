<?php

namespace TaylorThomas\WordPress\DekoForms;

abstract class AbstractValidator implements ValidatorInterface
{
    /** @var array $ */
    protected $data;

    /** @var array $ */
    protected $errors;

    /**
     * @param array $data
     */
    public function __construct(array $data)
    {
        $this->data   = $data;
        $this->errors = [];
    }
    
    /**
     * @return bool
     */
    public function getIsValid()
    {
        $this->validate();
        return empty($this->errors);
    }

    /**
     * @return array
     */
    public function getErrors()
    {
        return $this->errors;
    }

    /**
     * @return void
     */
    protected function validate()
    {
        $this->errors = [];
        $this->validatePresences();
    }

    /**
     * @param string $attrName
     * @return void
     */
    protected function validateEmailAddress(string $attrName)
    {
        if (!isset($this->errors[$attrName])) {
            $error = (new EmailValidator($this->data[$attrName]))->getError();
            if ($error) {
                $this->errors[$attrName] = $error;
            }
        }
    }

    /**
     * @return void
     */
    protected function validatePresences()
    {
        foreach ($this->getMandatories() as $mandatory) {
            if (!(isset($this->data[$mandatory]) && strlen(trim($this->data[$mandatory])))) {
                $this->errors[$mandatory] = ValidationErrorMessages::BLANK_MSG;
            }
        }
    }

    /**
     * @return array
     */
    abstract protected function getMandatories();
}
