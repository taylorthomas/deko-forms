<?php

namespace TaylorThomas\WordPress\DekoForms;

class EmailValidator
{
    const BLACKLISTED_MSG = 'must be a business address';

    const BLACKLISTED_DOMAIN_NAMES = [
        '163.com ',
        'aol.co.uk',
        'aol.com',
        'btconnect.com',
        'btinternet.com',
        'gmail.cm',
        'gmail.com',
        'googlemail.com',
        'hotmail.co.uk',
        'hotmail.com',
        'icloud.com',
        'live.co.uk',
        'live.com',
        'mail.com',
        'me.com',
        'outlook.co.uk',
        'outlook.com',
        'talktalk.co.uk',
        'talktalk.net',
        'wp.pl ',
        'yahoo.co.uk',
        'yahoo.com',
        'yahoo.fr ',
        'ymail.com'
    ];

    /** @var string $emailAddress */
    protected $emailAddress;

    /**
     * @param string $emailAddress
     */
    public function __construct(string $emailAddress)
    {
        $this->emailAddress = $emailAddress;
    }

    /**
     * @return string
     */
    public function getError()
    {
        if (!filter_var($this->emailAddress, FILTER_VALIDATE_EMAIL)) {
            return ValidationErrorMessages::INVALID_MSG;
        }

        if ($this->getIsBlacklisted()) {
            return self::BLACKLISTED_MSG;
        }
    }

    /**
     * @return bool
     */
    protected function getIsBlacklisted()
    {
        foreach (self::BLACKLISTED_DOMAIN_NAMES as $domainName) {
            if ($this->endsWith($this->emailAddress, $domainName)) {
                return true;
            }
        }
        return false;
    }

    /**
     * @param string $haystack
     * @param string $needle
     * @return bool
     */
    private function endsWith($haystack, $needle)
    {
        $length = strlen($needle);
        if ($length == 0) {
            return true;
        }
        return (substr($haystack, -$length) === $needle);
    }
}
